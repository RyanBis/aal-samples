package com.xiledsystems.aal.samples.ads.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.xiledsystems.aal.ads.AdInterface;
import com.xiledsystems.aal.ads.FullScreenAd;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.VCUtil;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.samples.ads.R;
import com.xiledsystems.aal.samples.ads.events.FullScreenAdEvent;
import com.xiledsystems.aal.samples.ads.model.FullScreenAdSample;
import com.xiledsystems.aal.util.TextUtil;
import com.xiledsystems.aal.util.ViewUtil;


public class FullScreenAdSampleActivity extends AalActivity implements EventListener {


    private Button showAd;
    private Button reloadAd;
    private TextView errorConsole;

    // Use the FullScreenAd class. There is no need to make any code changes
    // for using google/amazon ads. You just need to create productFlavors,
    // then ccompile against whichever module you want (see app/build.gradle).
    private FullScreenAd ad;

    // This is the Model class which will control all logic.
    private FullScreenAdSample model;

    // A class defined in the bottom of this file, which responds
    // to callbacks associated with the ad
    private AdListener adListener = new AdListener();


    @Override
    public void onCreateActivity(Bundle bundle) {
        setContentView(R.layout.fullscreen_ad_sample);

        // Instantiate our model class, then register an Event listener with it. Remember to
        // implement EventListener in your Activity
        model = new FullScreenAdSample(this);
        model.registerListener(this);

        // The ViewUtil class makes it easy to cast your view
        showAd = ViewUtil.findView(R.id.showAd, this);

        //The VCUtil class allows you to easily fire an event on common
        // view actions, like onClick()
        VCUtil.setClickEvent(showAd, FullScreenAdEvent.SHOW_AD_CLICKED);

        reloadAd = ViewUtil.findView(R.id.reload, this);
        VCUtil.setClickEvent(reloadAd, FullScreenAdEvent.RELOAD_AD_CLICKED);

        errorConsole = ViewUtil.findView(R.id.errorConsole, this);

        // Instantiate the ad objext, passing your activity, the resource Id of your
        // app key, and a listener
        ad = new FullScreenAd(this, getString(R.string.ad_app_id), adListener);
    }

    @Override
    public void onEvent(Event event) {
        // Get the category from the Event object, and make sure it's for
        // a FullScreenAdEvent. It will be null if it is not.
        FullScreenAdEvent action = event.getCategory(FullScreenAdEvent.class);
        if (action != null) {
            switch (action) {
                case RELOAD_AD:
                    // We'll reload the ad now
                    showAd.setEnabled(false);
                    // Clear out the error console
                    errorConsole.setText("");
                    ad.onDestroy();
                    ad.onCreate(FullScreenAdSampleActivity.this, getString(R.string.ad_app_id));
                    ad.loadAd();
                    break;
                case SHOW_ERROR:
                    showAd.setEnabled(false);
                    String error = TextUtil.string("Error with ad. Error msg: ", event.get(0));
                    errorConsole.setText(error);
                    break;
                case ENABLE_SHOW_AD:
                    showAd.setEnabled(true);
                    break;
                case SHOW_AD:
                    ad.showAd();
                    break;
            }
        }
    }

    private class AdListener implements AdInterface {
        @Override
        public void adClosed() {
            model.postEvent(FullScreenAdEvent.AD_CLOSED);
        }

        @Override
        public void adError(String error) {
            model.postEvent(FullScreenAdEvent.AD_ERROR, error);
        }

        @Override
        public void adLoaded() {
            model.postEvent(FullScreenAdEvent.AD_LOADED);
        }

        @Override
        public void adLoadingError(String error) {
            model.postEvent(FullScreenAdEvent.AD_LOAD_ERROR, error);
        }
    }

}
