package com.xiledsystems.aal.samples.ads.model;


import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.AalEntryModel;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.samples.ads.events.FullScreenAdEvent;


public class FullScreenAdSample extends AalEntryModel {


    public FullScreenAdSample(AalActivity context) {
        super(context);
        // Set the category for this model. Only events for this category will
        // call the processCategoryEvent() method. Because of this, you don't have
        // to check if it's null in that method.
        setCategory(FullScreenAdEvent.class);
    }

    /**
     * Return true here to shutdown the backing EventManager when this Screen is destroyed.
     */
    @Override
    public boolean killEventManagerOnDestroy() {
        return true;
    }

    @Override
    public boolean processCategoryEvent(Event args) {
        // Grab the Event category from the Event. This will not be null, so there's
        // no need to check if it's null (unlike processEvent()).
        FullScreenAdEvent action = args.getCategory(FullScreenAdEvent.class);
        switch (action) {
            case AD_CLOSED:
                // The postEvent method is just a convenience, which calls EventManager.postEvent()
                postEvent(FullScreenAdEvent.RELOAD_AD);
                return true;
            case AD_LOADED:
                postEvent(FullScreenAdEvent.ENABLE_SHOW_AD);
                return true;
            case AD_ERROR:
                postEvent(FullScreenAdEvent.SHOW_ERROR, args.get(0));
                return true;
            case AD_LOAD_ERROR:
                postEvent(FullScreenAdEvent.SHOW_ERROR, args.get(0));
                return true;
            case SHOW_AD_CLICKED:
                postEvent(FullScreenAdEvent.SHOW_AD);
                return true;
            case RELOAD_AD_CLICKED:
                postEvent(FullScreenAdEvent.RELOAD_AD);
            default:
                break;
        }
        return false;
    }

    @Override
    public Class<?> getActivityClass() {
        return null;
    }

}
