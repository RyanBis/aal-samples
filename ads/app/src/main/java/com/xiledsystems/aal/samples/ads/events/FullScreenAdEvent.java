package com.xiledsystems.aal.samples.ads.events;


public enum FullScreenAdEvent {
    AD_CLOSED,
    AD_LOADED,
    AD_ERROR,
    AD_LOAD_ERROR,
    SHOW_AD,
    SHOW_AD_CLICKED,
    RELOAD_AD,
    RELOAD_AD_CLICKED,
    ENABLE_SHOW_AD,
    SHOW_ERROR
}
