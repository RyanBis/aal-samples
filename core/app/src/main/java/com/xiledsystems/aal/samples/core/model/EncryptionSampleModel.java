package com.xiledsystems.aal.samples.core.model;


import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.AalScreenModel;
import com.xiledsystems.aal.encrypt.EncryptionType;
import com.xiledsystems.aal.encrypt.SecureComponent;
import com.xiledsystems.aal.encrypt.SecurePrefs;
import com.xiledsystems.aal.encrypt.SecureStore;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.samples.core.activities.EncryptionActivity;
import com.xiledsystems.aal.samples.core.encryption.Thing;
import com.xiledsystems.aal.samples.core.events.EncryptionEvent;
import com.xiledsystems.aal.util.StopWatch;
import com.xiledsystems.aal.util.ThreadManager;
import java.util.ArrayList;


public class EncryptionSampleModel extends AalScreenModel {


    private static final String TAG = "sometag_";
    private static final int AES_ARRAY_RES = com.xiledsystems.aal.R.array.aes_bitrates;
    private static final int RSA_ARRAY_RES = com.xiledsystems.aal.R.array.rsa_bitrates;

    private boolean isRsa = false;
    private int curBitrate = 128;
    private String[] bitRates;

    private SecurePrefs curPrefs;
    private SecureStore curStore;
    private ThreadManager tMgr;


    public EncryptionSampleModel(AalActivity context) {
        super(context, EncryptionEvent.class);

        bitRates = getContext().getResources().getStringArray(AES_ARRAY_RES);

        tMgr = ThreadManager.getInstance(true);

        final Event e = new Event(EncryptionEvent.UPDATE_BITRATE_DROPDOWN, 1);
        e.add(bitRates);
        postEvent(e);
        postEvent(EncryptionEvent.LOAD_STORAGE_INSTANCES, curBitrate);
    }

    public EncryptionSampleModel() {
        super();
    }

    @Override
    public Class<?> getActivityClass() {
        return EncryptionActivity.class;
    }

    @Override
    public boolean processCategoryEvent(Event args) {
        EncryptionEvent e = args.getCategory(EncryptionEvent.class);
        switch (e) {
            case BIT_RATE_SELECTED:
                int position = args.get(0);
                int reqBitRate = Integer.parseInt(bitRates[position]);
                if (curBitrate != reqBitRate) {
                    postEvent(EncryptionEvent.DISABLE_BUTTONS);
                    postEvent(EncryptionEvent.LOAD_STORAGE_INSTANCES, reqBitRate);
                }
                return true;
            case AES_SELECTED:
                return loadAes();
            case RSA_SELECTED:
                return loadRsa();
            case LOAD_STORAGE_INSTANCES:
                final Integer bitRate = args.get(0);
                this.curBitrate = bitRate;
                loadStorageInstances(getType(isRsa, bitRate));
                return true;
            case LOAD:
                loadData();
                return true;
            case DO_SAVE:
                save();
                return true;
        }
        return false;
    }

    private boolean loadRsa() {
        if (!isRsa) {
            isRsa = true;
            postEvent(EncryptionEvent.DISABLE_BUTTONS);
            bitRates = getContext().getResources().getStringArray(RSA_ARRAY_RES);
            final Event e1 = new Event(EncryptionEvent.UPDATE_BITRATE_DROPDOWN, 1);
            e1.add(bitRates);
            postEvent(e1);
            postEvent(EncryptionEvent.LOAD_STORAGE_INSTANCES, 256);
        }
        return true;
    }

    private boolean loadAes() {
        if (isRsa) {
            isRsa = false;
            postEvent(EncryptionEvent.DISABLE_BUTTONS);
            bitRates = getContext().getResources().getStringArray(AES_ARRAY_RES);
            final Event e = new Event(EncryptionEvent.UPDATE_BITRATE_DROPDOWN, 1);
            e.add(bitRates);
            postEvent(e);
            postEvent(EncryptionEvent.LOAD_STORAGE_INSTANCES, 128);
        }
        return true;
    }

    private void save() {
        tMgr.execute(new Runnable() {
            @Override
            public void run() {
                String someString = "Here's data with " + curPrefs.getEncryptionType().toString() + " encryption.";
                curPrefs.putString(TAG, someString);
                curPrefs.putInt(TAG + 1, curPrefs.getEncryptionType().getBitLength());
                curPrefs.putLong(TAG + 2, curPrefs.getEncryptionType().getBitLength() * 100000000L);
                curPrefs.putBoolean(TAG + 3, true);
                int size = 500;
                ArrayList<Thing> list = new ArrayList<>(size);
                for (int i = 0; i < size; i++) {
                    Thing t = new Thing();
                    t.count = i;
                    t.name = "Something #" + i;
                    list.add(t);
                }
                curStore.putSerializables(TAG, list);
                postEvent(EncryptionEvent.SAVE_COMPLETE);
            }
        });
    }

    private void loadData() {
        tMgr.execute(new Runnable() {
            @Override
            public void run() {
                final StopWatch s = new StopWatch();
                s.start();
                curStore.getSerializables(TAG, new SecureStore.StoreListener<Thing>() {
                    @Override
                    public void gotList(ArrayList<Thing> list) {
                        long diff = s.stop();
                        StringBuilder string = new StringBuilder();
                        s.start();
                        string.append(curPrefs.getString(TAG, "Nothing!"));
                        string.append("\n").append(s.stop()).append("\n");
                        s.start();
                        string.append(curPrefs.getInt(TAG + 1, -1)).append("\n");
                        string.append(s.stop()).append("\n");
                        s.start();
                        string.append(curPrefs.getLong(TAG + 2, -1)).append("\n");
                        string.append(s.stop()).append("\n");
                        s.start();
                        string.append(curPrefs.getBoolean(TAG + 3, false)).append("\n");
                        string.append(s.stop()).append("\n");
                        string.append(list.size()).append(" Things").append("\n");
                        string.append(diff).append("\n");
                        final String text = string.toString();
                        postEvent(EncryptionEvent.DATA_LOADED, text);
                    }
                });
            }
        });
    }

    private void loadStorageInstances(EncryptionType enc) {
        curPrefs = new SecurePrefs(getContext(), enc.toString(), enc);
        curPrefs.setLoggingEnabled(true);
        curStore = new SecureStore(getContext(), enc);
        curStore.setLoggingEnabled(true);
        tMgr.execute(new Runnable() {
            @Override
            public void run() {
                curPrefs.init(new SecureComponent.KeyListener() {
                    @Override
                    public void onKeyInitialized() {
                        curStore.init(new SecureComponent.KeyListener() {
                            @Override
                            public void onKeyInitialized() {
                                postEvent(EncryptionEvent.READY);
                            }
                        });
                    }
                });
            }
        });
    }

    private static EncryptionType getType(boolean isRsa, int bitRate) {
        switch (bitRate) {
            case 192:
                if (!isRsa) {
                    return EncryptionType.newAes192();
                }
            case 256:
                if (isRsa) {
                    return EncryptionType.newRsa256();
                } else {
                    return EncryptionType.newAes256();
                }
            case 512:
                if (isRsa) {
                    return EncryptionType.newRsa512();
                }
            case 1024:
                if (isRsa) {
                    return EncryptionType.newRsa1024();
                }
            case 2048:
                if (isRsa) {
                    return EncryptionType.newRsa2048();
                }
            case 4096:
                if (isRsa) {
                    return EncryptionType.newRsa4096();
                }
            default:
                if (isRsa) {
                    return EncryptionType.newRsa256();
                } else {
                    return EncryptionType.newAes128();
                }
        }
    }

}
