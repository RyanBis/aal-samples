package com.xiledsystems.aal.samples.core.model;


import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.AalEntryModel;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.samples.core.activities.MainActivity;
import com.xiledsystems.aal.samples.core.events.MenuEvents;
import java.util.HashMap;
import java.util.Map;


/**
 * This is the entry point to the core samples app, so it extends AalEntryModel. It's also the "exit" point.
 * By that, I mean that when this screen is destroyed, the app should be closed/shutdown.
 * When a sample is chosen, a new screen is shown for said sample. This screen does not get destroyed then,
 * only when you hit the back button when this screen is entered (visible on screen).
 */
public class MainModel extends AalEntryModel {


    /**
     * Map where the String key is the title of the sample, and the
     * Class<?> value is the class to go to.
     */
    private final static Map<String, Class<?>> SAMPLES = new HashMap<>();

    /**
     * populate the Map
     */
    static {
        SAMPLES.put("SqlDb Example #1", SqlDbSampleModel.class);
        SAMPLES.put("SoundPlayer Example", SoundPlayerSample.class);
        SAMPLES.put("Encryption Storage Sample", EncryptionSampleModel.class);
    }

    public MainModel(AalActivity context) {
        /**
         * The constructor below sets the category for this Model. This pertains to the {@link com.xiledsystems.aal.control.AalScreenModel#processCategoryEvent(Event)}
         * method. That method will only be called when an Event is posted with the category you set
         * with this constructor, or by manually calling {@link #setCategory(Class)}
         */
        super(context, MenuEvents.class);


        /**
         * Enable logging for the EventManager while debugging. Set to false for release (false is the
         * default)
         */
        EventManager.getInstance().setLoggingEnabled(true);
    }

    @Override
    public boolean processCategoryEvent(Event eventArgs) {
        /**
         * Get the category from the Event
         */
        MenuEvents event = eventArgs.getCategory(MenuEvents.class);
        switch (event) {
            case LOAD_SAMPLES:
                /**
                 * Send the samples data to the Activity, to fill in the listview with the available samples.
                 * When sending an array, you must explicitly use the {@link Event} class like shown below.
                 */
                Event args = new Event(MenuEvents.SAMPLES_LOADED, /*number of args*/1);
                String[] samples = SAMPLES.keySet().toArray(new String[SAMPLES.size()]);
                args.add(samples);
                postEvent(args);
                /**
                 * Returning true here means that no other listener will respond to this event.
                 */
                return true;
            case PICKED_SAMPLE:
                /**
                 * A sample was selected from the ListView, now open the selected sample
                 */
                String title = eventArgs.get(0);
                gotoScreen(SAMPLES.get(title));
                return true;
        }
        return false;
    }

    @Override
    public boolean killEventManagerOnDestroy() {
        // When this returns true, the EventManager will be shutdown when the Activity behind
        // this Controller calls onDestroy().
        return true;
    }

    @Override
    public Class<?> getActivityClass() {
        return MainActivity.class;
    }
}
