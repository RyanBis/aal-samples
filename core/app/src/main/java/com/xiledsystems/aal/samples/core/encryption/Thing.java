package com.xiledsystems.aal.samples.core.encryption;


import java.io.IOException;
import java.io.Serializable;

public class Thing implements Serializable {

    public String name = "";
    public int count = 0;

    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.writeUTF(name);
        out.writeInt(count);
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        name = in.readUTF();
        count = in.readInt();
    }

}
