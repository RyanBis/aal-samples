package com.xiledsystems.aal.samples.core.events;


public enum AddPetEvent {
    NAME_CHANGED,
    TYPE_CHANGED,
    BREED_CHANGED,
    ADD_PET,
    DISABLE_ADD,
    ENABLE_ADD,
    RESET_FIELDS
}
