package com.xiledsystems.aal.samples.core.sqldb;

import android.content.ContentValues;
import android.database.Cursor;
import com.xiledsystems.aal.sql.DBObject;


public class Blob extends DBObject {


    public static final String DATA = "data";
    public static final String TBL_BLOB = "blob_store";

    private byte[] data;


    public Blob() {
        super();
    }

    public Blob(Cursor cursor) {
        super(cursor);
    }


    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public void loadFromCursor(Cursor cursor) {
        data = cursor.getBlob(cursor.getColumnIndex(DATA));
    }

    @Override
    public String getTableName() {
        return TBL_BLOB;
    }

    @Override
    public ContentValues getCreateValues() {
        ContentValues value = new ContentValues(1);
        value.put(DATA, data);
        return value;
    }
}
