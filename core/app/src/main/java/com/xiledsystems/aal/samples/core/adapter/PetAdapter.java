package com.xiledsystems.aal.samples.core.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.xiledsystems.aal.samples.core.R;
import com.xiledsystems.aal.samples.core.sqldb.Pet;
import com.xiledsystems.aal.util.ViewUtil;
import java.util.List;


public class PetAdapter extends ArrayAdapter<Pet> {

    private List<Pet> pets;

    public PetAdapter(Context context, List<Pet> objects) {
        super(context, R.layout.pet_list_layout, objects);
        this.pets = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder v;
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.pet_list_layout, null);
            v = new ViewHolder();
            v.name = ViewUtil.find(R.id.name, convertView);
            v.type = ViewUtil.find(R.id.type, convertView);
            v.breed = ViewUtil.find(R.id.breed, convertView);
            convertView.setTag(v);
        } else {
            v = (ViewHolder) convertView.getTag();
        }
        final Pet p = pets.get(position);
        v.name.setText(p.getName());
        v.type.setText(p.getType());
        v.breed.setText(p.getBreed());
        return convertView;
    }

    private static class ViewHolder {
        TextView name;
        TextView type;
        TextView breed;
    }
}
