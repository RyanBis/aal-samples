package com.xiledsystems.aal.samples.core.activities;


import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.samples.core.R;
import com.xiledsystems.aal.samples.core.events.AddPetEvent;
import com.xiledsystems.aal.samples.core.model.AddPetModel;
import com.xiledsystems.aal.util.ViewUtil;


public class AddPetActivity extends AalActivity implements EventListener {


    private EditText name;
    private EditText type;
    private EditText breed;
    private Button add;

    private AddPetModel model;


    @Override
    public void onCreateActivity(Bundle savedInstanceState) {
        setContentView(R.layout.activity_addpet);
        model = new AddPetModel(this);
        model.registerListener(this);

        name = ViewUtil.find(R.id.name, this);
        setTextChangedEvent(name, AddPetEvent.NAME_CHANGED);
        type = ViewUtil.find(R.id.type, this);
        setTextChangedEvent(type, AddPetEvent.TYPE_CHANGED);
        breed = ViewUtil.find(R.id.breed, this);
        setTextChangedEvent(breed, AddPetEvent.BREED_CHANGED);
        add = ViewUtil.find(R.id.add, this);
        setClickEvent(add, AddPetEvent.ADD_PET);
    }

    @Override
    public void onEvent(Event event) {
        AddPetEvent e = event.getCategory(AddPetEvent.class);
        if (e != null) {
            switch (e) {
                case RESET_FIELDS:
                    name.setText("");
                    type.setText("");
                    breed.setText("");
                    add.setEnabled(false);
                    break;
                case ENABLE_ADD:
                    add.setEnabled(true);
            }
        }
    }
}
