package com.xiledsystems.aal.samples.core.model;


import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.AalScreenModel;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.samples.core.activities.AddPetActivity;
import com.xiledsystems.aal.samples.core.events.AddPetEvent;
import com.xiledsystems.aal.samples.core.sqldb.Blob;
import com.xiledsystems.aal.samples.core.sqldb.Pet;
import com.xiledsystems.aal.sql.DBSchema;
import com.xiledsystems.aal.sql.DataType;
import com.xiledsystems.aal.sql.SqlDb;
import com.xiledsystems.aal.sql.Table;
import com.xiledsystems.aal.ui.DialogHelper;
import com.xiledsystems.aal.util.TextUtil;


public class AddPetModel extends AalScreenModel {


    private String name;
    private String type;
    private String breed;

    private SqlDb db;


    public AddPetModel(AalActivity context) {
        super(context, AddPetEvent.class);
        initDb();
    }

    public AddPetModel() {
        super();
    }

    @Override
    public Class<?> getActivityClass() {
        return AddPetActivity.class;
    }

    @Override
    public boolean processCategoryEvent(Event args) {
        AddPetEvent event = args.getCategory(AddPetEvent.class);
        switch (event) {
            case NAME_CHANGED:
                name = args.get(0);
                checkToEnable();
                return true;
            case TYPE_CHANGED:
                type = args.get(0);
                checkToEnable();
                return true;
            case BREED_CHANGED:
                breed = args.get(0);
                checkToEnable();
                return true;
            case ADD_PET:
                addPet();
                return true;
        }
        return false;
    }

    private void initDb() {
        DBSchema scheme = new DBSchema();
        scheme.addTable(Pet.TBL_PETS, SqlDbSampleModel.PET_COLUMNS);
        Table blob = new Table(Blob.TBL_BLOB);
        blob.addColumn(Blob.DATA, DataType.BLOB);
        scheme.addTable(blob);

        db = new SqlDb(getViewController().getContext(), scheme);
        db.setLoggingEnabled(true);
    }

    private void addPet() {
        Pet pet = new Pet();
        pet.setBreed(breed);
        pet.setName(name);
        pet.setType(type);
        pet.insert(db);
        askToAddMore();
    }

    private void askToAddMore() {
        DialogHelper.showDialog(getContext(), "Add More?", "Would you like to add more pets?", new DialogHelper.DialogListener() {
            @Override
            public void buttonClicked(String buttonText) {
                if (buttonText.equals("No")) {
                    kill();
                } else {
                    postEvent(AddPetEvent.RESET_FIELDS);
                }
            }
        }, "Yes", "No");
    }

    private void checkToEnable() {
        if (!TextUtil.isEmpty(name) && !TextUtil.isEmpty(type) && !TextUtil.isEmpty(breed)) {
            postEvent(AddPetEvent.ENABLE_ADD);
        } else {
            postEvent(AddPetEvent.DISABLE_ADD);
        }
    }
}
