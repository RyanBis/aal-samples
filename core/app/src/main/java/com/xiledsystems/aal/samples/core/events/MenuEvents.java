package com.xiledsystems.aal.samples.core.events;


public enum MenuEvents {

    LOAD_SAMPLES,
    PICKED_SAMPLE,
    SAMPLES_LOADED,
    START_SAMPLE

}
