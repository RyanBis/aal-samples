package com.xiledsystems.aal.samples.core.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.samples.core.R;
import com.xiledsystems.aal.samples.core.adapter.PetAdapter;
import com.xiledsystems.aal.samples.core.events.SqlDbEvents;
import com.xiledsystems.aal.samples.core.model.SqlDbSampleModel;
import com.xiledsystems.aal.samples.core.sqldb.Pet;
import com.xiledsystems.aal.util.ViewUtil;
import java.util.ArrayList;


public class SqlDbSampleActivity extends AalActivity implements EventListener {


    private Button add;
    private Button list;
    private ListView listView;
    private PetAdapter adapter;

    private SqlDbSampleModel model;


    @Override
    public void onCreateActivity(Bundle bundle) {
        setContentView(R.layout.activity_sqldb1);

        model = new SqlDbSampleModel(this);
        model.registerListener(this);

        add = ViewUtil.find(R.id.add, this);
        setClickEvent(add, SqlDbEvents.ADD_CLICKED);
        list = ViewUtil.find(R.id.list, this);
        setClickEvent(list, SqlDbEvents.LIST_CLICKED);
        listView = ViewUtil.find(R.id.listView, this);

    }

    @Override
    public void onEvent(Event eventArgs) {
        SqlDbEvents event = eventArgs.getCategory(SqlDbEvents.class);
        if (event != null) {
            switch (event) {
                case POPULATE_LISTVIEW:
                    ArrayList<Pet> pets = eventArgs.get(0);
                    handleListViewPopulation(pets);
                    break;
            }
        }
    }

    private void handleListViewPopulation(ArrayList<Pet> pets) {
        if (adapter == null) {
            adapter = new PetAdapter(this, pets);
            listView.setAdapter(adapter);
        } else {
            adapter.clear();
            adapter.addAll(pets);
            adapter.notifyDataSetChanged();
        }
    }

}
