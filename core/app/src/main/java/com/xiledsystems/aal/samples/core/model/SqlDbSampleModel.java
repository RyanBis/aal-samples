package com.xiledsystems.aal.samples.core.model;


import android.content.Context;
import android.database.Cursor;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.AalScreenModel;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.samples.core.R;
import com.xiledsystems.aal.samples.core.activities.SqlDbSampleActivity;
import com.xiledsystems.aal.samples.core.events.SqlDbEvents;
import com.xiledsystems.aal.samples.core.sqldb.Blob;
import com.xiledsystems.aal.samples.core.sqldb.Pet;
import com.xiledsystems.aal.sql.DBObjectFactory;
import com.xiledsystems.aal.sql.DBSchema;
import com.xiledsystems.aal.sql.DataType;
import com.xiledsystems.aal.sql.Query;
import com.xiledsystems.aal.sql.ReturnedObject;
import com.xiledsystems.aal.sql.SqlDb;
import com.xiledsystems.aal.sql.SqlDbReturn;
import com.xiledsystems.aal.sql.Table;
import java.util.ArrayList;


public class SqlDbSampleModel extends AalScreenModel {


    protected static final String[] PET_COLUMNS = {Pet.NAME, Pet.TYPE, Pet.BREED};
    private static final byte[] DATA = {0x0, 0x1, 0x2, 0x4};

    private SqlDb db;


    public SqlDbSampleModel(AalActivity context) {
        super(context, SqlDbEvents.class);
        initDb();
        if (getPetCount() < 4) {
            loadDummyData();
        }
    }

    public SqlDbSampleModel() {
        super();
    }


    @Override
    public boolean processCategoryEvent(Event eventArgs) {
        SqlDbEvents event = eventArgs.getCategory(SqlDbEvents.class);
        switch (event) {
            case ADD_CLICKED:
                gotoScreen(AddPetModel.class);
                return true;
            case LIST_CLICKED:
                runQuery();
                return true;
        }
        return false;
    }

    private void runQuery() {
        Query q = new Query();
        q.select().from(Pet.TBL_PETS);
        q.getObjectListAsync(db, new DBObjectFactory<Pet>() {
            @Override
            public Pet newObject(Cursor cursor) {
                return new Pet(cursor);
            }
        }, new SqlDbReturn() {
            @Override
            public void objectReturned(ReturnedObject value) {
                ArrayList<Pet> pets = value.get();
                postEvent(SqlDbEvents.POPULATE_LISTVIEW, pets);
            }

            @Override
            public boolean returnOnUIThread() {
                return false;
            }
        });
    }

    private void initDb() {
        DBSchema scheme = new DBSchema();
        scheme.addTable(Pet.TBL_PETS, PET_COLUMNS);
        Table blob = new Table(Blob.TBL_BLOB);
        blob.addColumn(Blob.DATA, DataType.BLOB);
        scheme.addTable(blob);

        db = new SqlDb(getViewController().getContext(), scheme);
        db.setLoggingEnabled(true);
    }

    private long getPetCount() {
        Query q = new Query();
        q.selectCount().from(Pet.TBL_PETS);
        return q.getCount(db);
    }

    private void loadDummyData() {
        final Context context = getViewController().getContext();
        Pet jack = Pet.fromResource(context, R.array.jack);
        jack.insert(db);
        Pet chaos = Pet.fromResource(context, R.array.chaos);
        chaos.insert(db);
        Pet abby = Pet.fromResource(context, R.array.abby);
        abby.insert(db);
        Pet clive = Pet.fromResource(context, R.array.clive);
        clive.insert(db);

        Blob bob = new Blob();
        bob.setData(DATA);
        bob.insert(db);
    }


    @Override
    public void destroy() {
        super.destroy();
        db.clearTable(Pet.TBL_PETS);
        db.clearTable(Blob.TBL_BLOB);
    }

    @Override
    public Class<?> getActivityClass() {
        return SqlDbSampleActivity.class;
    }
}
