package com.xiledsystems.aal.samples.core.events;


public enum SoundEvents {
    ADD_SOUND,
    PLAY_SOUND,
    UNLOAD_SOUNDS,
    SOUNDS_INITIALIZED,
    SOUND_ERROR,
    SOUND_LOADED,
    PLAY_SONG
}
