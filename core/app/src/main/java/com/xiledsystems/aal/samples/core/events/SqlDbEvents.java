package com.xiledsystems.aal.samples.core.events;


public enum SqlDbEvents {

    INIT_DB,
    LOAD_DUMMY_DATA,
    ADD_CLICKED,
    LIST_CLICKED,
    POPULATE_LISTVIEW,


}
