package com.xiledsystems.aal.samples.core.activities;


import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.VCUtil;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.events.PostType;
import com.xiledsystems.aal.samples.core.R;
import com.xiledsystems.aal.samples.core.events.EncryptionEvent;
import com.xiledsystems.aal.samples.core.model.EncryptionSampleModel;
import com.xiledsystems.aal.ui.DropDown;
import com.xiledsystems.aal.util.ViewUtil;


public class EncryptionActivity extends AalActivity implements EventListener {


    private Button load;
    private Button save;

    private TextView textView;
    private DropDown bitRateDropDown;
    private RadioGroup encSelector;

    private EncryptionSampleModel model;

    private boolean manual = false;

    @Override
    public void onCreateActivity(Bundle savedInstanceState) {
        setContentView(R.layout.activity_encryption);

        model = new EncryptionSampleModel(this);
        model.registerListener(this);

        bitRateDropDown = new DropDown(this, R.id.bitRateSelector);
        bitRateDropDown.setonItemSelectListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!manual) {
                    model.postEvent(EncryptionEvent.BIT_RATE_SELECTED, position);
                } else {
                    manual = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        encSelector = ViewUtil.find(R.id.encSelector, this);
        encSelector.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (group.getCheckedRadioButtonId()) {
                    case R.id.rsa:
                        model.postEvent(EncryptionEvent.RSA_SELECTED);
                        break;
                    case R.id.aes:
                        model.postEvent(EncryptionEvent.AES_SELECTED);
                        break;
                }
            }
        });
        load = ViewUtil.find(R.id.load, this);
        load.setEnabled(false);
        setClickEvent(load, EncryptionEvent.LOAD);
        save = ViewUtil.find(R.id.save, this);
        save.setEnabled(false);
        setClickEvent(save, EncryptionEvent.SAVE_CLICKED);
        textView = ViewUtil.find(R.id.text, this);
    }

    @Override
    public void onEvent(Event event) {
        EncryptionEvent e = event.getCategory(EncryptionEvent.class);
        if (e != null) {
            switch (e) {
                case UPDATE_BITRATE_DROPDOWN:
                    manual = true;
                    String[] items = event.get(0);
                    bitRateDropDown.setItems(items);
                    break;
                case DISABLE_BUTTONS:
                    load.setEnabled(false);
                    save.setEnabled(false);
                    break;
                case SAVE_CLICKED:
                    load.setEnabled(false);
                    EventManager.getInstance().postEvent(PostType.onThread(), EncryptionEvent.DO_SAVE);
                    break;
                case SAVE_COMPLETE:
                    load.setEnabled(true);
                    break;
                case READY:
                    save.setEnabled(true);
                    break;
                case DATA_LOADED:
                    final String text = event.get(0);
                    textView.setText(text);
                    break;
            }
        }
    }
}
