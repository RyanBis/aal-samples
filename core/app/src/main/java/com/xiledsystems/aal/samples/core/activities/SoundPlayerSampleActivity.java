package com.xiledsystems.aal.samples.core.activities;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.VCUtil;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.samples.core.R;
import com.xiledsystems.aal.samples.core.events.SoundEvents;
import com.xiledsystems.aal.samples.core.model.SoundPlayerSample;
import com.xiledsystems.aal.util.ViewUtil;


public class SoundPlayerSampleActivity extends AalActivity implements EventListener {

    // Views
    private Button sound1Btn;
    private Button sound2Btn;
    private Button sound3Btn;
    private Button playSong;
    private Button load;
    private Button unload;

    // Controller
    private SoundPlayerSample control;

    @Override
    public void onCreateActivity(Bundle bundle) {

        setContentView(R.layout.activity_soundplayer);

        control = new SoundPlayerSample(this);
        control.registerListener(this);

        sound1Btn = ViewUtil.findView(R.id.sound1, this);
        VCUtil.setClickEvent(sound1Btn, SoundEvents.PLAY_SOUND, R.raw.from_behind);

        sound2Btn = ViewUtil.findView(R.id.sound2, this);
        VCUtil.setClickEvent(sound2Btn, SoundEvents.PLAY_SOUND, R.raw.galactic_distraction);

        sound3Btn = ViewUtil.findView(R.id.sound3, this);
        VCUtil.setClickEvent(sound3Btn, SoundEvents.PLAY_SOUND, R.raw.infrared);

        load = ViewUtil.findView(R.id.load, this);
        load.setEnabled(false);
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                load.setEnabled(false);
                control.postEvent(SoundEvents.ADD_SOUND, R.raw.from_behind, R.raw.galactic_distraction, R.raw.infrared);
            }
        });

        unload = ViewUtil.findView(R.id.unload, this);
        unload.setEnabled(false);
        unload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                control.postEvent(SoundEvents.UNLOAD_SOUNDS);
                unload.setEnabled(false);
                sound1Btn.setEnabled(false);
                sound2Btn.setEnabled(false);
                sound3Btn.setEnabled(false);
                load.setEnabled(true);
            }
        });

        playSong = ViewUtil.findView(R.id.song, this);
        VCUtil.setClickEvent(playSong, SoundEvents.PLAY_SONG);

    }

    @Override
    public void onEvent(Event eventArgs) {
        SoundEvents event = eventArgs.getCategory(SoundEvents.class);
        if (event != null) {
            switch (event) {
                case SOUNDS_INITIALIZED:
                    boolean success = eventArgs.get(0);
                    if (success) {
                        sound1Btn.setEnabled(true);
                        sound2Btn.setEnabled(true);
                        sound3Btn.setEnabled(true);
                        unload.setEnabled(true);
                        load.setEnabled(false);
                    }
                    break;
                case SOUND_ERROR:
                    // Do something on error
                    break;
                case SOUND_LOADED:
                    Integer res = eventArgs.get(0);
                    switch (res) {
                        case R.raw.from_behind:
                            sound1Btn.setEnabled(true);
                            break;
                        case R.raw.galactic_distraction:
                            sound2Btn.setEnabled(true);
                            break;
                        case R.raw.infrared:
                            sound3Btn.setEnabled(true);
                            break;
                        default:
                            break;
                    }
                    if (sound3Btn.isEnabled() && sound2Btn.isEnabled() && sound3Btn.isEnabled()) {
                        unload.setEnabled(true);
                    }
                    break;
            }
        }
    }

}
