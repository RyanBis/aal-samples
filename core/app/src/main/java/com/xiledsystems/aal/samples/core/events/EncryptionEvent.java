package com.xiledsystems.aal.samples.core.events;


public enum EncryptionEvent {
    LOAD,
    SAVE_CLICKED,
    RSA_SELECTED,
    AES_SELECTED,
    BIT_RATE_SELECTED,
    UPDATE_BITRATE_DROPDOWN,
    DISABLE_BUTTONS,
    LOAD_STORAGE_INSTANCES,
    READY,
    DO_SAVE,
    SAVE_COMPLETE,
    DATA_LOADED
}
