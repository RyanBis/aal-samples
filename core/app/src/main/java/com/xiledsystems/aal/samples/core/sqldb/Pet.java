package com.xiledsystems.aal.samples.core.sqldb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.xiledsystems.aal.sql.DBObject;


/**
 * Class which represents a Pet, which is stored in the {@link com.xiledsystems.aal.sql.SqlDb} database.
 */
public class Pet extends DBObject {

    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String BREED = "breed";
    public static final String TBL_PETS = "pets";

    private String name;
    private String type;
    private String breed;


    public Pet() {
        // Make sure to always call super()
        super();
    }

    public Pet(Cursor cursor) {
        super(cursor);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        addData(NAME, name);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        addData(TYPE, type);
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
        addData(BREED, breed);
    }

    @Override
    public void loadFromCursor(Cursor cursor) {
        // When using the constructor DBObject(Cursor), this method
        // is called, which loads the class from the database.
        // There are some convenience methods to get data from the
        // cursor, like getCursorString() that is used below
        name = getCursorString(NAME, cursor);
        type = getCursorString(TYPE, cursor);
        breed = getCursorString(BREED, cursor);
    }

    @Override
    public String getTableName() {
        return TBL_PETS;
    }

    @Override
    public ContentValues getCreateValues() {
        // Ths method is called when an instance of this class is about to get inserted
        // into the database.
        ContentValues values = new ContentValues();
        values.put(NAME, name);
        values.put(TYPE, type);
        values.put(BREED, breed);
        return values;
    }

    // Method to load string arrays from resources to create
    // some pre-made pets
    public static Pet fromResource(Context context, int resId) {
        Pet p = new Pet();
        String[] pet = context.getResources().getStringArray(resId);
        p.setName(pet[0]);
        p.setType(pet[1]);
        p.setBreed(pet[2]);
        return p;
    }
}
