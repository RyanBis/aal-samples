package com.xiledsystems.aal.samples.core.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.samples.core.R;
import com.xiledsystems.aal.samples.core.events.MenuEvents;
import com.xiledsystems.aal.samples.core.model.MainModel;
import com.xiledsystems.aal.util.ViewUtil;


/**
 * ViewController class for the MainModel screen. This class simply loads the UI,
 * and passes events back and forth from UI components to the MainModel.
 */
public class MainActivity extends AalActivity implements EventListener {


    /**
     * A ListView to display the available samples
     */
    private ListView sampleList;

    /**
     * The model for the Main screen.
     */
    private MainModel model;


    @Override
    public void onCreateActivity(Bundle bundle) {
        setContentView(R.layout.activity_main);
        /**
         * Use the {@link ViewUtil} class to easily cast your views
         */
        sampleList = ViewUtil.find(R.id.listView, this);

        /**
         * Instantiate the MainModel class. This must be done in this onCreateActivity method.
         */
        model = new MainModel(this);

        /**
         * Register this Activity as an {@link EventListener}, to respond to posted {@link Event}s
         */
        model.registerListener(this);

        /**
         * Post an {@link Event} to load the available samples
         */
        model.postEvent(MenuEvents.LOAD_SAMPLES);
    }

    @Override
    public void onEvent(Event event) {
        /**
         * Get the category from the posted {@link Event}.
         */
        MenuEvents menuEvent = event.getCategory(MenuEvents.class);

        /**
         * If the {@link Event} posted was not a MenuEvents category, then null
         * will be returned.
         */
        if (menuEvent != null) {
            switch (menuEvent) {
                /**
                 * The samples have been loaded, now we can populate the ListView.
                 */
                case SAMPLES_LOADED:
                    String[] samples = event.get(0);
                    loadAdapter(samples);
                    break;
            }
        }
    }

    private void loadAdapter(String[] samples) {
        /**
         * Populate our ListView with the given samples String array.
         */
        final ArrayAdapter<String> adaptor = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, samples);
        sampleList.setAdapter(adaptor);
        sampleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /**
                 * Post an {@link Event} that the sample has been picked, and send the title back in the {@link Event}.
                 */
                model.postEvent(MenuEvents.PICKED_SAMPLE, adaptor.getItem(position));
            }
        });
    }

}
