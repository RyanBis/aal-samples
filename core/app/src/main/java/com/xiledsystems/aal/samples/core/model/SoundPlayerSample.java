package com.xiledsystems.aal.samples.core.model;

import android.content.Context;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.control.AalScreenModel;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.media.SongPlayer;
import com.xiledsystems.aal.media.SoundCallbacks;
import com.xiledsystems.aal.media.SoundPlayer;
import com.xiledsystems.aal.samples.core.R;
import com.xiledsystems.aal.samples.core.activities.SoundPlayerSampleActivity;
import com.xiledsystems.aal.samples.core.events.SoundEvents;


public class SoundPlayerSample extends AalScreenModel {

    private SoundPlayer player;
    private SongPlayer songer;


    public SoundPlayerSample(AalActivity context) {
        super(context, SoundEvents.class);
        initPlayers();
    }

    public SoundPlayerSample() {
        super();
    }

    private void initPlayers() {
        Context context = getContext();
        player = new SoundPlayer(context);
        player.setLoggingEnabled(true);
        player.setSoundCallbacks(new SoundListener());
        player.addSound(R.raw.from_behind);
        player.addSound(R.raw.galactic_distraction);
        player.addSound(R.raw.infrared);
        player.initialize();

        songer = new SongPlayer(context);
        songer.setSoundResource(R.raw.galactic_distraction);
        songer.load();
    }

    @Override
    public boolean processCategoryEvent(Event args) {
        SoundEvents action = args.getCategory(SoundEvents.class);
        switch (action) {
            case PLAY_SOUND:
                Integer resId = args.get(0);
                player.play(resId);
                return true;
            case ADD_SOUND:
                int size = args.size();
                for (int i = 0; i < size; i++) {
                    Integer soundRes = args.get(i);
                    if (soundRes != null) {
                        player.addSound(soundRes);
                    }
                }
                return true;
            case UNLOAD_SOUNDS:
                player.unloadAll();
                return true;
            case PLAY_SONG:
                songer.play();
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public Class<?> getActivityClass() {
        return SoundPlayerSampleActivity.class;
    }

    private class SoundListener implements SoundCallbacks {
        @Override
        public void onInitialized(boolean success) {
            postEvent(SoundEvents.SOUNDS_INITIALIZED, success);
        }

        @Override
        public void onError(SoundPlayer.SoundErrors soundErrors) {
            postEvent(SoundEvents.SOUND_ERROR, soundErrors);
        }

        @Override
        public void onSoundLoaded(int res) {
            postEvent(SoundEvents.SOUND_LOADED, res);
        }
    }
}
